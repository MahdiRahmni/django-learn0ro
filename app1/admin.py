from django.contrib import admin
from .models import  HeaderTitle, Navbar, Nav_Tow, Object_Nav_Tow,About_My,Blog,Blog_Item
from .models import Object_Blog_1, Object_Blog_2, Object_Blog_3, Object_Blog_4,donate,Comment_blog_tab1
from .models import Comment_course,Comment_blog_tab2,Comment_blog_tab3,Comment_blog_tab4

class PostAdmin(admin.ModelAdmin):
     list_display=("Title","Date","Date_Moon","Image")
     list_editable=("Title","Image")
     list_display_links=("Date",)
     list_filter=("Date_Moon",)
     search_fields=("Title","Image","Text")
class Titr(admin.ModelAdmin):
    list_display=("Header","Color_Header","Titr","Continue_Titr")
    list_editable=("Header","Color_Header")
    list_display_links=("Titr",)
class nav(admin.ModelAdmin):
    list_display=("One","Two","Tree","For")
    list_editable=("One","Two","Tree",)
    list_display_links=("For",)
class header(admin.ModelAdmin):
    list_display= ("BigTitle","Title","Link")
    list_editable=("BigTitle","Title")
    list_display_links=("Link",)
class bl_item(admin.ModelAdmin):
    list_display=("Item",)
    list_editable=("Item",)
    list_display_links=(None)
class ObNa_2(admin.ModelAdmin):
    list_display=("Title","Lower_Title",)
    list_editable=("Title",)
    list_display_links=("Lower_Title",)
    list_filter=("Date_Moon",)
    search_fields=("Title","Lower_Title","Text")
class BlogAdmin(admin.ModelAdmin):
    list_display=("Color_Titel","Titr","Continue_Titr")
    list_editable=("Color_Titel","Titr","Continue_Titr")
    list_display_links=(None)

admin.site.register(HeaderTitle, header)
admin.site.register(Navbar, nav)
admin.site.register(Nav_Tow, Titr)
admin.site.register(Object_Nav_Tow, ObNa_2)
admin.site.register(About_My)
admin.site.register(Blog, BlogAdmin)
admin.site.register(Blog_Item, bl_item)
admin.site.register(Object_Blog_1, PostAdmin)
admin.site.register(Object_Blog_2, PostAdmin)
admin.site.register(Object_Blog_3, PostAdmin)
admin.site.register(Object_Blog_4, PostAdmin)
admin.site.register(donate)
admin.site.register(Comment_blog_tab1)
admin.site.register(Comment_blog_tab2)
admin.site.register(Comment_blog_tab3)
admin.site.register(Comment_blog_tab4)
admin.site.register(Comment_course)

