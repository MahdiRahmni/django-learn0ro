from django.forms import ModelForm
from . import models
from captcha.fields import CaptchaField


class comment (ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = models.Comment_course
        fields = ('post','author','text')

class Comment_blog_tab1 (ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = models.Comment_blog_tab1
        fields = ('post','author','text')

class Comment_blog_tab2 (ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = models.Comment_blog_tab2
        fields = ('post','author','text')

class Comment_blog_tab3 (ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = models.Comment_blog_tab3
        fields = ('post','author','text')

class Comment_blog_tab4 (ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = models.Comment_blog_tab4
        fields = ('post','author','text')
