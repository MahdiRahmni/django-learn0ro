# Generated by Django 2.2.8 on 2020-08-09 19:32

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='Name',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='Text',
        ),
        migrations.RemoveField(
            model_name='object_blog_1',
            name='Comments',
        ),
        migrations.AddField(
            model_name='comment',
            name='approved_comment',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='comment',
            name='author',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='comment',
            name='post',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='app1.Object_Blog_1'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='comment',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(default=''),
        ),
    ]
