# Generated by Django 2.2.8 on 2020-08-11 14:30

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0006_remove_comment_course_approved_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment_course',
            name='author',
            field=models.CharField(default='نویسنده', max_length=200),
        ),
        migrations.AlterField(
            model_name='comment_course',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(default='متن ...'),
        ),
    ]
