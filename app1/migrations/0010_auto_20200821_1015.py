# Generated by Django 2.2.8 on 2020-08-21 05:45

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0009_remove_headertitle_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='object_blog_1',
            name='T_Code',
            field=models.CharField(default='', max_length=200, verbose_name='Titr With Code'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='object_blog_1',
            name='T_Text',
            field=models.CharField(default='', max_length=200, verbose_name='Titr Main Text'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='object_blog_1',
            name='Text',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Main Text'),
        ),
    ]
