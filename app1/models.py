from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from ckeditor_uploader.fields import RichTextUploadingField
from django.forms import ModelForm
from django import forms

class HeaderTitle(models.Model):
	BigTitle = models.CharField('top title',max_length=60,help_text="۳ تا باید بسازی نه کمتر نه بیشتر")
	Title = models.CharField('title',max_length=100)
	Link = models.CharField('لینک',max_length=120,help_text="<h2>لطفا نام صفحات داخلی را وارد کنید<h2>")
	def __str__(self):
		return self.BigTitle
class Navbar(models.Model):
	One  = models.CharField('one title',default='Home',max_length=20)
	Two  = models.CharField('two title',default='Package',max_length=20)
	Tree = models.CharField('tree title',default='About Me',max_length=20)
	For  = models.CharField('for title',default='Blog',max_length=20)
	def __str__(self):
		return str(self.id)
class Nav_Tow(models.Model):
	Header       = models.CharField('Header',max_length=20)
	Color_Header = models.CharField('Color Header',max_length=20)
	Titr		 = models.CharField('Titr',max_length=40)
	Continue_Titr= models.CharField('Continue Titr',max_length=40)
	def __str__(self):
		return self.Header
class Object_Nav_Tow(models.Model):
	Title = models.CharField('Title',max_length=30)
	Lower_Title = models.CharField('Lower_Title',max_length=30,
									blank=True,null=True,
									help_text="اگر زیاده تیترت خالی بذار")
	Text = RichTextUploadingField(('content'),config_name='default')
	Complit_text = RichTextUploadingField(('توضیحات کامل'))
	Date = models.TimeField('date')
	Date_Moon= models.DateField('Date_Moon')
	Image = models.ImageField('image')	
	Image_bg =	models.ImageField('عکس پس زمینه',blank=True,null=True,)	
	Course = RichTextUploadingField(('<div class="row "><div class="col-sm-12 col-md-6 col-lg-12 col-xl-6 mb-4 "><div class="media tm-bg-transparent-black tm-border-white  "><i class=" tm-icon-media rtl"></i><div class="media-body rtl"><h3>WRITE TITEL<h3><p> WRITE TEXT </br><a href="WRITE LINK" target="_blank"> دیدن فیلم   </a></p></div></div></div><!--------------------------------------><div class="col-sm-12 col-md-6 col-lg-12 col-xl-6 mb-4 "><div class="media tm-bg-transparent-black tm-border-white"><i class=" mr-4 tm-icon-media"></i><div class="media-body"><h3>WRITE TITEL</h3><p> WRITE TEXT  </br><a href="WRITE LINK" target="_blank"> دیدن فیلم  </a></p></div></div></div></div>'))
	def __str__(self):
		return self.Title
class Object_Nav_Tree(models.Model):
	Title = models.CharField('Title',max_length=30)
	Lower_Title = models.CharField('Lower_Title',max_length=30,
									blank=True,null=True,
									help_text="اگر زیاده تیترت خالی بذار")
	Text = RichTextUploadingField(('content'),config_name='default')
	def __str__(self):
		return self.Title
class About_My(models.Model):
	Title = models.CharField('نوشته قبل رنگ نوشته',max_length=100)
	Company_name= models.CharField('رنگ نوشته',max_length=30)
	Continue_Title = models.CharField('ادامه نوشته',max_length=100)
	Text = RichTextUploadingField(('متن زیر نوشته اصلی'),config_name='default',)
	Name = models.CharField("نام ادمین", max_length=25 ,)
	Date = models.TimeField('زمان')
	Date_Moon= models.DateField('تاریخ')
	link = models.CharField("متن لینک بغل",max_length=100)
	Image = models.ImageField('آپلود عکس',blank=True,null=True,)
	content = models.TextField('خلاصه',max_length=350,blank=True,null=True,)
	Text_2 = RichTextUploadingField(('متن اصلی'),config_name='default',blank=True,null=True,)
	A_blog = RichTextUploadingField(('درباره بلاگ'),config_name='default',blank=True,null=True)
	Skill_text =  RichTextUploadingField(('مهارت ها'),config_name='default',blank=True,null=True,)
	Projects =  RichTextUploadingField(('پروژه ها'),config_name='default',blank=True,null=True,)
	N_file1 = models.CharField("نام فایل 1",max_length=100,blank=True,null=True,)
	File1 = models.FileField("1 آپلود فایل",blank=True,null=True,)
	N_file2 = models.CharField("نام فایل 2",max_length=100,blank=True,null=True,)
	File2 = models.FileField("2 آپلود فایل",blank=True,null=True,)
	N_file3 = models.CharField("نام فایل 3",max_length=100,blank=True,null=True,)
	File3 = models.FileField("3 آپلود فایل",blank=True,null=True,)
	N_file4 = models.CharField("نام فایل 4",max_length=100,blank=True,null=True,)
	File4 = models.FileField("4 آپلود فایل",blank=True,null=True,)
	Phone = models.CharField(' phone' , max_length=15,blank=True,null=True,)
	E_mail = models.EmailField(unique=True,blank=True,null=True,)
	def __str__(self):
		return self.Name
class Blog(models.Model):
	Color_Titel  = models.CharField('Color Title', max_length=50)
	Titr		 = models.CharField('Titr',max_length=40)
	Continue_Titr= models.CharField('Continue Titr',max_length=40)

	def __str__(self):
		return self.Color_Titel
class Blog_Item(models.Model):
	Item = models.CharField('please enter the items',default='Item',max_length=25)
	def __str__(self):
		return self.Item
class Object_Blog_1(models.Model):
	Title = models.CharField('titel',max_length=200)
	Date = models.TimeField('date')
	Date_Moon= models.DateField('Date_Moon')
	Image = models.ImageField('image',blank=True,null=True,)
	content = models.TextField('content',max_length=300,blank=True,null=True,)
	
	Titr = models.CharField('Continue Titr',max_length=200)
	Code_Text = RichTextUploadingField(('Code discribtion '),config_name='default',blank=True,null=True,)
	T_Code = models.CharField('Titr With Code',max_length=200)
	Code  = models.TextField('Code',blank=True,null= True)
	T_Text = models.CharField('Titr Main Text',max_length=200)
	Text = RichTextUploadingField(('Main Text'),config_name='default',blank=True,null=True,)		
	Quote = RichTextUploadingField('Quote',blank=True,null=True)
	Name_Quote = models.CharField(max_length=30,blank=True,null=True,)
	
	Movi_Text = RichTextUploadingField(('txt video'),config_name='default',blank=True,null=True,);
	Movi = models.FileField(blank=True,null=True,)
	filename = models.CharField(max_length=250,blank=True,null=True,)
	T = models.CharField('Hed_Tit',max_length=20,blank=True,null=True,)
	def __str__(self):
		return self.Title
class Object_Blog_2(models.Model):
	Title = models.CharField('titel',max_length=200)
	Date = models.TimeField('date')
	Date_Moon= models.DateField('Date_Moon')
	Image = models.ImageField('image',blank=True,null=True,)
	content = models.TextField('content',max_length=300,blank=True,null=True,)
	
	Titr = models.CharField('Continue Titr',max_length=200)
	Code_Text = RichTextUploadingField(('Code discribtion '),config_name='default',blank=True,null=True,)
	T_Code = models.CharField('Titr With Code',max_length=200)
	Code  = models.TextField('Code',blank=True,null= True)
	T_Text = models.CharField('Titr Main Text',max_length=200)
	Text = RichTextUploadingField(('Main Text'),config_name='default',blank=True,null=True,)		
	Quote = RichTextUploadingField('Quote',blank=True,null=True)
	Name_Quote = models.CharField(max_length=30,blank=True,null=True,)
	
	Movi_Text = RichTextUploadingField(('txt video'),config_name='default',blank=True,null=True,);
	Movi = models.FileField(blank=True,null=True,)
	filename = models.CharField(max_length=250,blank=True,null=True,)
	T = models.CharField('Hed_Tit',max_length=20,blank=True,null=True,)
	def __str__(self):
		return self.Title
class Object_Blog_3(models.Model):
	Title = models.CharField('titel',max_length=200)
	Date = models.TimeField('date')
	Date_Moon= models.DateField('Date_Moon')
	Image = models.ImageField('image',blank=True,null=True,)
	content = models.TextField('content',max_length=300,blank=True,null=True,)
	
	Titr = models.CharField('Continue Titr',max_length=200)
	Code_Text = RichTextUploadingField(('Code discribtion '),config_name='default',blank=True,null=True,)
	T_Code = models.CharField('Titr With Code',max_length=200)
	Code  = models.TextField('Code',blank=True,null= True)
	T_Text = models.CharField('Titr Main Text',max_length=200)
	Text = RichTextUploadingField(('Main Text'),config_name='default',blank=True,null=True,)		
	Quote = RichTextUploadingField('Quote',blank=True,null=True)
	Name_Quote = models.CharField(max_length=30,blank=True,null=True,)
	
	Movi_Text = RichTextUploadingField(('txt video'),config_name='default',blank=True,null=True,);
	Movi = models.FileField(blank=True,null=True,)
	filename = models.CharField(max_length=250,blank=True,null=True,)
	T = models.CharField('Hed_Tit',max_length=20,blank=True,null=True,)
	def __str__(self):
		return self.Title
class Object_Blog_4(models.Model):
	Title = models.CharField('titel',max_length=200)
	Date = models.TimeField('date')
	Date_Moon= models.DateField('Date_Moon')
	Image = models.ImageField('image',blank=True,null=True,)
	content = models.TextField('content',max_length=300,blank=True,null=True,)
	
	Titr = models.CharField('Continue Titr',max_length=200)
	Code_Text = RichTextUploadingField(('Code discribtion '),config_name='default',blank=True,null=True,)
	T_Code = models.CharField('Titr With Code',max_length=200)
	Code  = models.TextField('Code',blank=True,null= True)
	T_Text = models.CharField('Titr Main Text',max_length=200)
	Text = RichTextUploadingField(('Main Text'),config_name='default',blank=True,null=True,)		
	Quote = RichTextUploadingField('Quote',blank=True,null=True)
	Name_Quote = models.CharField(max_length=30,blank=True,null=True,)
	
	Movi_Text = RichTextUploadingField(('txt video'),config_name='default',blank=True,null=True,);
	Movi = models.FileField(blank=True,null=True,)
	filename = models.CharField(max_length=250,blank=True,null=True,)
	T = models.CharField('Hed_Tit',max_length=20,blank=True,null=True,)
	def __str__(self):
		return self.Title
class donate (models.Model):
	Titel = models.CharField('عنوان',max_length=120)
	Text = RichTextUploadingField(('توضیحات کامل'))
	def __str__(self):
		return self.Titel
class Comment_blog_tab1(models.Model):
    post = models.ForeignKey('app1.Object_Blog_1', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(default='نویسنده',max_length=200)
    text = RichTextUploadingField(default='متن ...')
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()
class Comment_blog_tab2(models.Model):
    post = models.ForeignKey('app1.Object_Blog_2', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(default='نویسنده',max_length=200)
    text = RichTextUploadingField(default='متن ...')
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()
class Comment_blog_tab3(models.Model):
    post = models.ForeignKey('app1.Object_Blog_3', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(default='نویسنده',max_length=200)
    text = RichTextUploadingField(default='متن ...')
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()
class Comment_blog_tab4(models.Model):
    post = models.ForeignKey('app1.Object_Blog_4', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(default='نویسنده',max_length=200)
    text = RichTextUploadingField(default='متن ...')
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()		
class Comment_course(models.Model):
    post = models.ForeignKey('app1.Object_Nav_Tow', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(default='نویسنده',max_length=200)
    text = RichTextUploadingField(default='متن ...')

    def approve(self):
        self.approved_comment = True
        self.save()
