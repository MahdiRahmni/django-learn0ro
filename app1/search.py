from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from .models import HeaderTitle,Navbar,Nav_Tow,Object_Nav_Tow,About_My,Blog,Blog_Item
from .models import Object_Blog_1, Object_Blog_2, Object_Blog_3, Object_Blog_4,donate


def Search(request):
    query = request.GET.get('search')
    if query :
        s1  = HeaderTitle.objects.all()
        search_result_1 = s1.filter(
            Q(BigTitle__icontains=query)|
            Q(Title__icontains=query)
        )

        if search_result_1:
            return {'search_result_1':search_result_1,}
        else :
            return {'search_result_1':'',}
    else:
        return  {'search_result_1' : None,}
# ===============================================================
def Search2(request):
    query = request.GET.get('search')
    if query :
            s2  = Object_Nav_Tow.objects.all()
            search_result_2 = s2.filter(
                Q(Title__icontains=query)|
                Q(Text__icontains=query)|
                Q(Complit_text__icontains=query)|
                Q(Course__icontains=query)
            )

            if search_result_2:
                return {'search_result_2':search_result_2,}
            else :
                return {'search_result_2':'',}
    else:
        return  {'search_result_2' : None,}
# ===============================================================
def Search3(request):
    query = request.GET.get('search')
    if query :
            s3  = Object_Blog_1.objects.all()
            search_result_3 = s3.filter(
                Q(Title__icontains=query)|
                Q(content__icontains=query)|
                Q(Text__icontains=query)|
                Q(Code_Text__icontains=query)|
                Q(Code__icontains=query)|
                Q(Quote__icontains=query)|
                Q(Name_Quote__icontains=query)|
                Q(Movi_Text__icontains=query)|
                Q(filename__icontains=query)|
                Q(T__icontains=query)
            )

            if search_result_3:
                return {'search_result_3':search_result_3,}
            else :
                return {'search_result_3':'',}
    else:
        return  {'search_result_3' : None,}
# ===============================================================
def Search4(request):
    query = request.GET.get('search')
    if query :
            s4  = Object_Blog_2.objects.all()
            search_result_4 = s4.filter(
                Q(Title__icontains=query)|
                Q(content__icontains=query)|
                Q(Text__icontains=query)|
                Q(Code_Text__icontains=query)|
                Q(Code__icontains=query)|
                Q(Quote__icontains=query)|
                Q(Name_Quote__icontains=query)|
                Q(Movi_Text__icontains=query)|
                Q(filename__icontains=query)|
                Q(T__icontains=query)
            )

            if search_result_4:
                return {'search_result_4':search_result_4,}
            else :
                return {'search_result_4':'',}

    else:
        return  {'search_result_4' : None,}
# ===============================================================
def Search5(request):
    query = request.GET.get('search')
    if query :
            s5  = Object_Blog_3.objects.all()
            search_result_5 = s5.filter(
                Q(Title__icontains=query)|
                Q(content__icontains=query)|
                Q(Text__icontains=query)|
                Q(Code_Text__icontains=query)|
                Q(Code__icontains=query)|
                Q(Quote__icontains=query)|
                Q(Name_Quote__icontains=query)|
                Q(Movi_Text__icontains=query)|
                Q(filename__icontains=query)|
                Q(T__icontains=query)
            )

            if search_result_5:
                return {'search_result_5':search_result_5,}
            else :
                return {'search_result_5':'',}

    else:
        return  {'search_result_5' : None,}
# ===============================================================
def Search6(request):
    query = request.GET.get('search')
    if query :
            s6  = Object_Blog_4.objects.all()
            search_result_6 = s6.filter(
                Q(Title__icontains=query)|
                Q(content__icontains=query)|
                Q(Text__icontains=query)|
                Q(Code_Text__icontains=query)|
                Q(Code__icontains=query)|
                Q(Quote__icontains=query)|
                Q(Name_Quote__icontains=query)|
                Q(Movi_Text__icontains=query)|
                Q(filename__icontains=query)|
                Q(T__icontains=query)
            )

            if search_result_6:
                return {'search_result_6':search_result_6,}
            else :
                return {'search_result_6':'',}

    else:
        return  {'search_result_6' : None,}
# ===============================================================
def Search7(request):
    query = request.GET.get('search')
    if query :
            s7  = About_My.objects.all()
            search_result_7 = s7.filter(
                Q(Title__icontains=query)|
                Q(Continue_Title__icontains=query)|
                Q(Text__icontains=query)|
                Q(content__icontains=query)|
                Q(Text_2__icontains=query)|
                Q(A_blog__icontains=query)|
                Q(Skill_text__icontains=query)|
                Q(Projects__icontains=query)|
                Q(N_file1__icontains=query)|
                Q(N_file2__icontains=query)|
                Q(N_file3__icontains=query)|
                Q(N_file4__icontains=query)|
                Q(E_mail__icontains=query)
            )

            if search_result_7:
                return {'search_result_7':search_result_7,}
            else :
                return {'search_result_7':'',}

    else:
        return  {'search_result_7' : None,}
# ===============================================================
def Search8(request):
    query = request.GET.get('search')
    if query :
            s8  = donate.objects.all()
            search_result_8 = s8.filter(
                Q(Text__icontains=query)
            )

            if search_result_8:
                return {'search_result_8':search_result_8,}
            else :
                return {'search_result_8':'',}

    else:
        return  {'search_result_8' : None,}
