from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    # path('admin/', admin.site.urls),
    url(r'^$',views.index,name="index"),
    url(r'^$',views.comment_in_era,name="comment_in_era"),
    url(r'^about$',views.about,name= "about"),
    url(r'^Donate$',views.Donate,name= "Donate"),
    url(r'^Search$',views.search,name= "Search"),
    url(r'^(?P<id>[0-9]{1,2})$',views.tab1,name='tab1'),
    url(r'^(?P<id>[0-9]{2,4})$',views.tab2,name='tab2'),
    url(r'^(?P<id>[0-9]{4,6})$',views.tab3,name='tab3'),
    url(r'^(?P<id>[0-9]{6,8})$',views.tab4,name='tab4'),
    url(r'^(?P<id>[0-9]{8,10})$',views.course,name='course'),

]
