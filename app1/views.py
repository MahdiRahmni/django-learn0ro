from django.shortcuts import render, get_object_or_404
from .models import HeaderTitle,Navbar,Nav_Tow,Object_Nav_Tow,About_My,Blog,Blog_Item
from .models import Object_Blog_1, Object_Blog_2, Object_Blog_3, Object_Blog_4,donate
from django.http import HttpResponseRedirect
from django.template import RequestContext
from . import forms
from .forms import comment,Comment_blog_tab1,Comment_blog_tab2,Comment_blog_tab3,Comment_blog_tab4


def index(request):
	titles = HeaderTitle.objects.all
	part_tow = Nav_Tow.objects.all
	obj_part_tow = Object_Nav_Tow.objects.all
	navs = Navbar.objects.all
	About = About_My.objects.all
	blog = Blog.objects.all
	blog_part1 = Object_Blog_1.objects.all
	blog_part2 = Object_Blog_2.objects.all
	blog_part3 = Object_Blog_3.objects.all
	blog_part4 = Object_Blog_4.objects.all
	items = Blog_Item.objects.all
	dic ={
		'titles':titles, 'part_tow':part_tow , 'obj_part_tow':obj_part_tow,
		'navs':navs, 'About':About, 'blog':blog, 'items':items,
		'blog_part1':blog_part1, 'blog_part2':blog_part2 , 'blog_part3':blog_part3,
		'blog_part4':blog_part4,
	}
	return render(request,'index.html',dic)
def tab1(request,id=None):
	post = get_object_or_404(Object_Blog_1,id=id)
	ID_Plas = int(id)-1
	items = Blog_Item.objects.all()
	about = About_My.objects.all()
	form = Comment_blog_tab1(request.POST or None)
	if form.is_valid():
		form.save()
		# messages.success(request,"با موفقیت ذخیره شد")
		return HttpResponseRedirect('#')
	context = {
		"post":post,'items':items,'next':ID_Plas,'about':about,'form':form,
	}
	return render(request, 'tab1.html', context)
def tab2 (request,id =None):
	post = get_object_or_404(Object_Blog_2,id=id)
	ID_Plas = int(id)-1
	items = Blog_Item.objects.all
	about = About_My.objects.all
	form = Comment_blog_tab2(request.POST or None)
	if form.is_valid():
		form.save()
		# messages.success(request,"با موفقیت ذخیره شد")
		return HttpResponseRedirect('#')
	context = {
		"post":post,'items':items,"next":ID_Plas,'about':about,'form':form,
	}
	return render(request, 'tab2.html',context)
def tab3 (request,id =None):
		post = get_object_or_404(Object_Blog_3,id=id)
		ID_Plas = int(id)-1
		items = Blog_Item.objects.all
		about = About_My.objects.all
		form = Comment_blog_tab3(request.POST or None)
		if form.is_valid():
			form.save()
			# messages.success(request,"با موفقیت ذخیره شد")
			return HttpResponseRedirect('#')
		context = {
			"post":post,'items':items,"next":ID_Plas,'about':about,'form':form,
		}
		return render(request, 'tab3.html',context)
def tab4 (request,id =None):
	post = get_object_or_404(Object_Blog_4,id=id)
	ID_Plas = int(id)-1
	items = Blog_Item.objects.all
	about = About_My.objects.all
	form = Comment_blog_tab4(request.POST or None)
	if form.is_valid():
		form.save()
		# messages.success(request,"با موفقیت ذخیره شد")
		return HttpResponseRedirect('#')
	context = {
		"post":post,'items':items,"next":ID_Plas,'about':about,'form':form,
	}
	return render(request, 'tab4.html',context)
def about(request):
	about = About_My.objects.all
	dic = {
		'about':about,
	}
	return render(request,'about.html',dic)
def course(request,id =None):
	post = get_object_or_404(Object_Nav_Tow,id=id)
	form = comment(request.POST or None)
	if form.is_valid():
		form.save()
		return HttpResponseRedirect('')
	context={
		'post':post,'form':form,
	}
	return render(request,'course.html',context)
def Donate(request):
	text = donate.objects.all
	dic = {
		'text':text,
	}
	return render(request,'Donate.html',dic)
def search (request):
	return render(request,'search.html')
def comment_in_era (request):
	return render(request,'comment_in_era.html')
